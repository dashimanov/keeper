﻿using System.IO;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        #region Export

        static void Export()
        {
            /********************************************************************************
             * Если в каталоге Export есть файл gmmq.packedge.end,
             * тогда выполняем скрипт на смещение шедулера на 3 минуты, и перезапускаем службы
             * если файл gmmq.packedge.end отсутсвует, очищаем каталог 
             * и выполняем скрипты на выгрузку реплики и смещение шедулера.
             *******************************************************************************/

            string pathFileExportG = "C:\\GMMQ\\Export\\gmmq.package.end";

            if (File.Exists(pathFileExportG))
            {
                //TODO: перед релизной сборкой снять комментарий
                //ServicesRestart restart = new ServicesRestart();
                //restart.RestartGmmq();
                //restart.RestartScheduler();

                var script = new ExecuteScript();
                script.ScriptOffset();
            }
            else
            {
                var delete = new DeleteFolerFiles();
                delete.DeleteFolder();

                var script = new ExecuteScript();
                script.ScriptExport();
                script.ScriptOffset();

                //ServicesRestart services = new ServicesRestart();
                //services.RestartGmmq();
                //services.RestartScheduler();
            }
        }

        #endregion

        #region Import

        static void Import()
        {
            /********************************************************************************
            * Если в каталоге Import есть файл gmmq.packedge.end,
            * тогда выполняем скрипт на всасывание реплики
            * если файл gmmq.packedge.end отсутсвует, очищаем каталог 
            * и выполняем скрипты на выгрузку реплики и смещение шедулера.
            *******************************************************************************/

            string pathFileImport = "C:\\GMMQ\\Import\\gmmq.package.end";

            if (File.Exists(pathFileImport))
            {
                //Всасываем реплику
                var script = new ExecuteScript();
                script.ScriptImport();

                //TODO: перед релизной сборкой снять комментарий
                //Restart
                //ServicesRestart services = new ServicesRestart();
                //services.RestartScheduler();
                //services.RestartGmmq();
            }
            else
            {
                //Чистим каталог
                var delete = new DeleteFolerFiles();
                delete.CleanFolderImport();

                //Делаем экспорт + смещение шедулера на 3 минуты
                var script = new ExecuteScript();
                script.ScriptExport();
                script.ScriptOffset();

                //TODO: перед релизной сборкой снять комментарий
                //ServicesRestart services = new ServicesRestart();
                //services.RestartScheduler();
                //services.RestartGmmq();
            }
        }

        #endregion

        static void Main(string[] args)
        {
            //Иницилизация log4net
            Logger.InitLogger();

            //Создаем 2 потока
            Thread thread = new Thread(new ThreadStart(Import));
            thread.Start();
            Export();

            Import();
        }
    }
}

