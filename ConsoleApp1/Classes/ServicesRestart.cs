﻿using System.ServiceProcess;
using ConsoleApp1.Base;

namespace ConsoleApp1
{
    public class ServicesRestart : IServiceRestart
    {
        /// <summary>
        /// Restart GMMQ service
        /// </summary>
        public int RestartGmmq()
        {
            ServiceController gmmq = new ServiceController("GMMQ");
            if (gmmq.Status == ServiceControllerStatus.Running)
            {
                gmmq.Stop();
                gmmq.WaitForStatus(ServiceControllerStatus.Stopped);
                Logger.Log.Info("Останавливаем службу GMMQ");

                gmmq.Start();
                gmmq.WaitForStatus(ServiceControllerStatus.Running);
                Logger.Log.Info("Запускаем службу GMMQ");
                //Перезапущен GMMQ
                return 10;
            }
            else
            {
                gmmq.Start();
                gmmq.WaitForStatus(ServiceControllerStatus.Running);
                Logger.Log.Info("Служба GMMQ была остановлена. Запустил службу...");
                //GMMQ не был запущен, но теперь запущен
                return 11;
            }
        }

        /// <summary>
        /// Restart GM_Scheduler process
        /// </summary>
        public int RestartScheduler()
        {
            ServiceController sheduler = new ServiceController("GM_SchedulerSvc");
            if (sheduler.Status == ServiceControllerStatus.Running)
            {
                sheduler.Stop();
                sheduler.WaitForStatus(ServiceControllerStatus.Stopped);
                Logger.Log.Info("Останавливаем службу GM_SchedulerSvc");

                sheduler.Start();
                sheduler.WaitForStatus(ServiceControllerStatus.Running);
                Logger.Log.Info("Запускаем службу GM_SchedulerSvc");

                //Sheduler перезапущен
                return 20;
            }
            else
            {
                sheduler.Start();
                sheduler.WaitForStatus(ServiceControllerStatus.Running);
                Logger.Log.Info("Служба GM_SchedulerSvc была остановлена. Запустил службу...");

                //Sheduler не был запущен, но теперь запущен
                return 21;
            }
        }
    }
}
