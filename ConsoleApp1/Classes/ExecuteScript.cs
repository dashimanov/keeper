﻿using System;
using System.Data;
using System.Data.SqlClient;
using ConsoleApp1.Base;

namespace ConsoleApp1
{
    public class ExecuteScript : IExecuteScript
    {
        /// <summary>
        /// Получаем имя ПК
        /// </summary>
        private string NamePc()
        {
            //Получаем имя ПК
            //TODO: перед релизной сборкой вернуть раскомментировать строку ниже
            //string namePc =  Environment.MachineName.ToLower();
            string namePc = "R54-630024-N";

            //Получаем индекс
            string[] index = namePc.Split(new char[] {'-'});
            string dataDaseName = "DB" + index[1];

            //Возвращаем имя ДБ
            return dataDaseName;
        }

        /// <summary>
        /// Execute script Export
        /// </summary>
        public void ScriptExport()
        {
            //Подключаемся к БД
            SqlConnection connection = new SqlConnection("Server = localhost; "
                                                         + "Initial Catalog = " + NamePc() + ";"
                                                         + "Integrated Security = SSPI");

            SqlCommand sqlCommand = new SqlCommand
            {
                CommandText = "use " + NamePc() + " exec ReplicaExport 0",
                CommandType = CommandType.Text,
                Connection = connection
            };
            try
            {
                connection.Open();
                Logger.Log.Info("Успешно подключились к БД " + NamePc() + "для выполнения скрипта ReplicaExport");
                sqlCommand.CommandTimeout = 240;
                sqlCommand.ExecuteNonQuery();
                connection.Close();
                Logger.Log.Info("Успешно выполнил скрипт ReplicaExport на БД " + NamePc() + "соединение закрыл");
            }
            catch (Exception exception)
            {
                Logger.Log.Error(exception.Message);
            }

        }

        /// <summary>
        /// Execite script Import
        /// </summary>
        public void ScriptImport()
        {
            //Подключаемся к БД
            SqlConnection connection = new SqlConnection("Server = localhost; "
                                                         + "Initial Catalog = " + NamePc() + ";"
                                                         + "Integrated Security = SSPI");

            SqlCommand sqlCommand = new SqlCommand
            {
                CommandText = "use " + NamePc() + " exec ReplicaImport 0",
                CommandType = CommandType.Text,
                Connection = connection
            };
            try
            {
                connection.Open();
                Logger.Log.Info("Успешно подключились к БД " + NamePc() + "для выполнения скрипта ReplicaImport");
                sqlCommand.CommandTimeout = 240;
                sqlCommand.ExecuteNonQuery();
                connection.Close();
                Logger.Log.Info("Успешно выполнил скрипт ReplicaImport на БД " + NamePc() + "соединение закрыл");
            }
            catch (Exception exception)
            {
                Logger.Log.Error(exception.Message);
            }

        }

        /// <summary>
        /// Смещение времени шедулера
        /// </summary>
        public void ScriptOffset()
        {
            //Подключаемся к БД
            SqlConnection connection = new SqlConnection("Server = localhost; "
                                                         + "Initial Catalog = " + NamePc() + ";"
                                                         + "Integrated Security = SSPI");
            SqlCommand sqlCommand = new SqlCommand
            {
                CommandText = "use " + NamePc()
                              +
                              " declare @increment int = 3\r\nbegin tran\r\nif(1 = (select 1 from GM_POSSchedJobTable where status = 0 and taskId = \'Репликация\'))\r\nbegin\r\n       update GM_POSSchedJobTable set StartDateTimeScheduled = dateAdd(mi, @increment, getdate()) where status = 0 and taskId = \'Репликация\'\r\n       select \'Время старта репликации изменено на \' + convert(nvarchar(60), dateAdd(mi, @increment, getdate()), 20)\r\n       commit tran\r\nend\r\nelse\r\nif(1=(select 1 from GM_POSSchedJobTable where status = 1 and taskId = \'Репликация\'))\r\nbegin\r\n       select \'Задание репликации находится в процессе выполнения. Повторите попытку через пару минут.\'\r\n       rollback tran\r\nend\r\nelse\r\nbegin\r\n       select \'Задание репликации отсутствует. Обратитесь к администратору.\'\r\n       rollback tran\r\nend",
                CommandType = CommandType.Text,
                Connection = connection
            };
            try
            {
                connection.Open();
                Logger.Log.Info("Успешно подключились к БД " + NamePc() + "для выполнения скрипта Offset");
                sqlCommand.ExecuteReader();
                connection.Close();
                Logger.Log.Info("Успешно выполнил скрипт Offset на БД " + NamePc() + "соединение закрыл");
            }
            catch (Exception exception)
            {
                Logger.Log.Error(exception.Message);
            }
        }
    }
}
