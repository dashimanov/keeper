﻿namespace ConsoleApp1.Base
{
    public interface IExecuteScript
    {
        void ScriptExport();
        
        void ScriptOffset();

        void ScriptImport();
    }
}