﻿namespace ConsoleApp1.Base
{
    public interface IDeleteFolderFiles
    {
        int DeleteFolder();

        int CleanFolderImport();
    }
}