﻿namespace ConsoleApp1.Base
{
   public interface IServiceRestart
    {
        int RestartGmmq();
        int RestartScheduler();
    }
}