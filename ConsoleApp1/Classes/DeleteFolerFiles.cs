﻿using System.IO;
using ConsoleApp1.Base;

namespace ConsoleApp1
{
    public class DeleteFolerFiles : IDeleteFolderFiles
    {
        /// <summary>
        /// Очистка каталога Export
        /// </summary>
        public int DeleteFolder()
        {
            DirectoryInfo directory = new DirectoryInfo(@"C:\GMMQ\Export");
            foreach (FileInfo file in directory.GetFiles())
            {
                file.Delete();
                Logger.Log.Info("Из каталога Export удалены файлы");

                //Удалены все файлы
                return 40;
            }
            foreach (DirectoryInfo file in directory.GetDirectories())
            {
                file.Delete(true);
                Logger.Log.Info("Из каталога Export удалены подкаталоги");

                //Удалены каталоги
                return 41;
            }

            return 42;
        }

        /// <summary>
        /// Очистка каталога Import
        /// </summary>
        /// <returns></returns>
        public int CleanFolderImport()
        {
            DirectoryInfo directory = new DirectoryInfo(@"C:\GMMQ\Import");
            foreach (FileInfo file in directory.GetFiles())
            {
                file.Delete();
                Logger.Log.Info("Из каталога Import удалены файлы");

                //Удалены все файлы
                return 40;
            }
            foreach (DirectoryInfo file in directory.GetDirectories())
            {
                file.Delete(true);
                Logger.Log.Info("Из каталога Import удалены подкаталоги");

                //Удалены каталоги
                return 41;
            }
            return 42;
        }
    }
}
